--mysql -u root

-- List down the databases inside the DBMS
SHOW DATABASES;

--Create a database
CREATE DATABASE music_db;

--Drop a database
DROP DATABASE music_db;

--Recreate a database
CREATE DATABASE music_db;

--Select a database
USE music_db;

--Create a table
-- Table columns have the following format: [column_name] [data_type] [other_options]

CREATE TABLE users(

	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL, 
	fullname VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)
);

CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL, 
	date_released DATE NOT NULL,
	artist_id INT NOT NULL, 
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);


CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlist_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE playlists_songs(
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE reviews(
	id INT NOT NULL AUTO_INCREMENT,
	review VARCHAR(300) NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

-- To display all the tables in a specific database
SHOW TABLES;

-- To remove a specific table
DROP TABLE reviews;

--show the details of the table in a table format
--DESCRIBE <table_name>
DESCRIBE reviews;

--ALTER TABLES
--to drop a column/field from a table
-- ALTER TABLE <table_name> DROP COLUMN <field_name>;
ALTER TABLE reviews DROP COLUMN review;


-- to add a new field in a table
--ALTER TABLE <table_name> ADD <new_field> <DATA TYPE> NOT NULL;
ALTER TABLE reviews ADD comments VARCHAR(300) NOT NULL;

--to add a new field in a table in a specific placement 
--ALTER TABLE <table_name> ADD <new_field> <DATA TYPE> NOT NULL AFTER <field_name>
ALTER TABLE reviews ADD feedback VARCHAR(300) NOT NULL AFTER id;

--to change the name of a specific field
--ALTER TABLE <table_name> CHANGE COLUMN <old_name> <new_name> <DATA TYPE> NOT NULL;
ALTER TABLE reviews CHANGE COLUMN comments review VARCHAR(250) NOT NULL;
